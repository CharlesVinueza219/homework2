package ui;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import settings.InitializationParameters;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;


/**
 * @author Charles Vinueza
 * this input dialog was first created because I didn't know fileChooser.showSaveDialog existed
 */
public class InputDialogSingleton extends Stage{
    static InputDialogSingleton singleton;

    VBox   messagePane;
    Scene  messageScene;
    TextField text;
    Button submitButton;
    Button cancelButton;
    String saveText;

    private InputDialogSingleton() { }

    /**
     * A static accessor method for getting the singleton object.
     *
     * @return The one singleton dialog of this object type.
     */
    public static InputDialogSingleton getSingleton() {
        if (singleton == null)
            singleton = new InputDialogSingleton();
        return singleton;
    }

    /**
     * This function fully initializes the singleton dialog for use.
     *
     * @param owner The window above which this dialog will be centered.
     */
    public void init(Stage owner) {
        // MAKE IT MODAL
        initModality(Modality.WINDOW_MODAL);
        initOwner(owner);

        // LABEL TO DISPLAY THE CUSTOM MESSAGE
        text = new TextField();
        text.setPromptText("Save as");
        text.setFocusTraversable(false);
        // CLOSE BUTTON
        cancelButton = new Button("Cancel");
        cancelButton.setOnAction(e -> this.close());

        submitButton = new Button("Save");
        // WE'LL PUT EVERYTHING HERE
        messagePane = new VBox();
        messagePane.setAlignment(Pos.CENTER);
        messagePane.getChildren().add(text);
        messagePane.getChildren().add(cancelButton);
        messagePane.getChildren().add(submitButton);


        // MAKE IT LOOK NICE
        messagePane.setPadding(new Insets(80, 60, 80, 60));
        messagePane.setSpacing(20);

        // AND PUT IT IN THE WINDOW
        messageScene = new Scene(messagePane);
        this.setScene(messageScene);

        submitButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent e) {
                if ((text.getText() != null && !text.getText().isEmpty())) {
                    System.out.println("Thanks for your comment");
                    saveText= text.getText();
                    close();


                } else {
                }
            }
        });



    }

    /**
     * This method loads a custom message into the label and
     * then pops open the dialog.
     *
     * @param title   The title to appear in the dialog window.
     */
    public void show(String title) {
        // SET THE DIALOG TITLE BAR TITLE
        setTitle(title);

        // SET THE MESSAGE TO DISPLAY TO THE USER

        // AND OPEN UP THIS DIALOG, MAKING SURE THE APPLICATION
        // WAITS FOR IT TO BE RESOLVED BEFORE LETTING THE USER
        // DO MORE WORK.
        showAndWait();
    }


}
