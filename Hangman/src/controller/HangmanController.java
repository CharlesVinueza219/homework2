package controller;

import apptemplate.AppTemplate;
import data.GameData;
import data.GameDataFile;
import gui.Workspace;
import javafx.animation.AnimationTimer;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.Node;
import javafx.stage.FileChooser;
import propertymanager.PropertyManager;
import ui.AppMessageDialogSingleton;
import ui.YesNoCancelDialogSingleton;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;
import static settings.AppPropertyType.*;

/**
 * @author Charles Vinueza
 * @author Ritwik Banerjee
 */
public class HangmanController implements FileController {

    static private AppTemplate appTemplate; // shared reference to the application


    private static GameData     gamedata;    // shared reference to the game being played, loaded or saved
    private Text[]              progress;    // reference to the text area for the word
    private boolean             success;     // whether or not player was successful
    private static int          discovered;  // the number of letters already discovered
    private Button              gameButton;  // shared reference to the "start game" button
    private Label               remains;     // dynamically updated label that indicates the number of remaining guesses
    private Label               letterUsedLabel;     // dynamically updated label that indicates the number of remaining guesses

    private static boolean      beingPlayed;
    private boolean             gameover;    // whether or not the current game is already over
    private static boolean      savable;
    private Path                workFile;
    public File                 currentWorkFile;
    private static boolean loadedFromNull = true;
    public static ObservableList<Node> letters;
    public static ObservableList<Node> lettersUsed;

    private Button hintButton;


    public SimpleBooleanProperty saved;

    public HangmanController(AppTemplate appTemplate, Button gameButton, Button hintButton) {
        this(appTemplate);
        this.gameButton = gameButton;
        this.hintButton = hintButton;
    }

    public HangmanController(AppTemplate appTemplate) {
        this.appTemplate = appTemplate;
    }

    public void enableGameButton() {
        if (gameButton == null) {
            Workspace workspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameButton = workspace.getStartGame();
        }
        gameButton.setDisable(false);
    }
    public void disableHintButton()
    {
        if(hintButton == null)
        {
            Workspace workspace = (Workspace) appTemplate.getWorkspaceComponent();
            hintButton = workspace.getHintButton();
        }
        hintButton.setDisable(true);
    }
    public void enableHintButton()
    {
        if(hintButton == null)
        {
            Workspace workspace = (Workspace) appTemplate.getWorkspaceComponent();
            hintButton = workspace.getHintButton();
        }
        hintButton.setDisable(false);
    }

    private static class Letter extends StackPane
    {
        private Rectangle rt = new Rectangle(40,60);
        private Text text;
        public Letter(char letter) {
            rt.setFill(Color.WHITE);
            rt.setStroke(Color.BLACK);
            text = new Text(String.valueOf(letter).toUpperCase());
            text.setFont(new Font("Times New Roman", 36));
            text.setVisible(false);
            setAlignment(Pos.CENTER);
            getChildren().addAll(rt, text);
        }
        public void show()
        {
            text.setVisible(true);
        }
        public void showEnd()
        {
            text.setFill(Color.RED);
            text.setVisible(true);
        }
        public boolean isEqualTo(char other) {
            return text.getText().equals(String.valueOf(other).toUpperCase());
        }

    }
    private static class BadLetterUsed extends StackPane
    {
        private Rectangle rect = new Rectangle(40,60);
        private Text text;
        public BadLetterUsed(char letter) {
            rect.setFill(Color.TRANSPARENT);
            text = new Text(String.valueOf(letter).toUpperCase());
            text.setFont(new Font("Times New Roman", 30));
            text.setFill(Color.RED);
            text.setVisible(true);
            setAlignment(Pos.CENTER);
            getChildren().addAll(rect, text);
        }
        public void reset()
        {
            for(Node n: lettersUsed)
            {
                BadLetterUsed a = (BadLetterUsed) n;
                n.setVisible(false);
            }
        }
    }
    private static class GoodLetterUsed extends StackPane
    {
        private Rectangle rect = new Rectangle(40,60);
        private Text text;
        public GoodLetterUsed(char letter) {
            rect.setFill(Color.TRANSPARENT);
            text = new Text(String.valueOf(letter).toUpperCase());
            text.setFont(new Font("Times New Roman", 30));
            text.setFill(Color.GREEN);
            text.setVisible(true);
            setAlignment(Pos.CENTER);
            getChildren().addAll(rect, text);
        }
        public void reset()
        {
            for(Node n: lettersUsed)
            {
                GoodLetterUsed a = (GoodLetterUsed) n;
                n.setVisible(false);
            }
        }
    }

    public void start() {
        gamedata = new GameData(appTemplate);
        gameover = false;
        beingPlayed = true;
        success = false;
        savable = true;
        discovered = 0;
        gameButton.setDisable(true);
        Workspace.hanger.setVisible(true);
        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        appTemplate.getGUI().updateWorkspaceToolbar(savable);
        HBox remainingGuessBox = gameWorkspace.getRemainingGuessBox();
        remains = new Label(Integer.toString(GameData.TOTAL_NUMBER_OF_GUESSES_ALLOWED));
        remainingGuessBox.getChildren().addAll(new Label("Remaining Bad Guesses: "), remains);
        HBox lettersUsedLabel = gameWorkspace.getLettersUsedLabel();
        letterUsedLabel = new Label(Integer.toString(10));
        lettersUsedLabel.getChildren().addAll(new Label("Letters used: "));
        System.out.println(gamedata.getTargetWord());

        for(char c: gamedata.getTargetWord().toCharArray())
        {
            letters.add(new Letter(c));
        }
        initWordGraphics();
        int uniqueChars = countUniqueChars(gamedata.getTargetWord());
        if(uniqueChars > 7 && gamedata.isAlreadyGivenHint() == false) {
            enableHintButton();
            hintButton.setVisible(true);
        }
        else
            disableHintButton();
        gameWorkspace.hanger.reset();
        play();
    }

    private static int countUniqueChars (String buf) {
        HashSet<Character> hash = new HashSet<>();
        buf = buf.toUpperCase();
        for (int i = 0; i < buf.length(); i++)
            hash.add(buf.charAt(i));
        return hash.size();
    }

    public void startWhenLoaded()
    {
        try {
            gamedata = GameDataFile.loadedData;
            gamedata.setAppTemplate(appTemplate);
            gameover = false;
            beingPlayed = true;
            success = false;
            savable = true;
            Workspace.hanger.setVisible(true);
            discovered = gamedata.getNumDiscovered();
            gameButton.setDisable(true);
            Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
            appTemplate.getGUI().updateWorkspaceToolbar(savable);
            HBox remainingGuessBox = gameWorkspace.getRemainingGuessBox();
            remains = new Label(Integer.toString(gamedata.getRemainingGuesses()));
            remainingGuessBox.getChildren().addAll(new Label("Remaining Bad Guesses: "), remains);
            HBox lettersUsedLabel = gameWorkspace.getLettersUsedLabel();
            letterUsedLabel = new Label(Integer.toString(10));
            lettersUsedLabel.getChildren().addAll(new Label("Letters used: "));
            Workspace.hanger.reset();
            initWordGraphics();
            for (char c : gamedata.getTargetWord().toCharArray()) {
                letters.add(new Letter(c));
            }
            for (char c : gamedata.getGoodGuesses()) {
                for (Node n : letters) {
                    Letter letter = (Letter) n;
                    if (letter.isEqualTo(c)) {
                        letter.show();
                    }
                }
            }
            for (char c : gamedata.getBadGuesses()) {
                Workspace.hanger.takeAwayLife();
            }
            for(char c: gamedata.getBadGuesses())
            {
                lettersUsed.add(new BadLetterUsed(c));
            }
            for(char c: gamedata.getGoodGuesses())
            {
                lettersUsed.add(new GoodLetterUsed(c));
            }
            int uniqueChars = countUniqueChars(gamedata.getTargetWord());
            if (uniqueChars > 7 && gamedata.isAlreadyGivenHint() == false) {
                enableHintButton();
                hintButton.setVisible(true);
            } else {
                hintButton.setVisible(true);
                disableHintButton();

            }

            play();

        }
        catch(Exception e)
        {
            AppMessageDialogSingleton error = AppMessageDialogSingleton.getSingleton();
            error.show("Load error", "An error occurred loading the game");
        }
    }

    public void handleHint() {
        /**
         * In start method, check if the targetword has more than 7 unique letters
         * If it does hintButton.setDisable(false)
         * hintbutton.setOnMouseClicked -> handleHint
         */
        gamedata.setAlreadyGivenHint(true);
        disableHintButton();
        char[] targetWordCharArr = gamedata.getTargetWord().toCharArray();
        ArrayList<Character> arrList = new ArrayList<Character>();
        for (char a : targetWordCharArr) {
            if (!gamedata.getGoodGuesses().contains(a)) {
                arrList.add(a);
            }
        }
        Random rand = new Random();
        int randomIndex = rand.nextInt((arrList.size()));
        char hint = arrList.get(randomIndex);

        int count = 0;
        for (Node n : letters) {
            Letter letter = (Letter) n;
            if (letter.isEqualTo(hint)) {
                letter.show();
                discovered++;
                System.out.println("Discovered is: " + discovered);
                gamedata.addGoodGuess(hint);
                gamedata.addBadGuess(hint);


                Workspace.hanger.takeAwayLife();
                if (count == 0) {
                    lettersUsed.add(new GoodLetterUsed(hint));
                    remains.setText(Integer.toString(gamedata.getRemainingGuesses()));
                }
                count = 1;

            }
        }

        success = (discovered == gamedata.getTargetWord().length());
            //remains.setText(Integer.toString(gamedata.getRemainingGuesses()));
            if (gamedata.getRemainingGuesses() <= 0 || success)
                end();
        }


    private void end() {

        if(success)
        {
            AppMessageDialogSingleton win = AppMessageDialogSingleton.getSingleton();
            win.show("Congrats", "You win!");

        }
        else {
            for (Node n : letters) {
                Letter letter = (Letter) n;
                for (char c : gamedata.getTargetWord().toCharArray()) {
                    if (alreadyGuessed(c) == false) {
                        if (letter.isEqualTo(c)) {
                            letter.showEnd();
                        }
                    }

                }
            }
        }
        //AppMessageDialogSingleton lose = AppMessageDialogSingleton.getSingleton();
        //lose.show(":(", "Ah, close but not quite there. The word was " + gamedata.getTargetWord());
        appTemplate.getGUI().getPrimaryScene().setOnKeyTyped(null);
        gameover = true;
        appTemplate.getDataComponent().reset();                // reset the data (should be reflected in GUI)
        gameButton.setDisable(true);
        savable = false; // cannot save a game that is already over
        disableHintButton();
        appTemplate.getGUI().updateWorkspaceToolbar(savable);

    }

    private void initWordGraphics()
    {
        char[] targetword = gamedata.getTargetWord().toCharArray();

/**
 progress = new Text[targetword.length];
 for (int i = 0; i < progress.length; i++) {

 progress[i] = new Text(Character.toString(targetword[i]));
 progress[i].setVisible(false);
 }
 guessedLetters.getChildren().addAll(progress);
 **/
    }

    public void play()
    {
        AnimationTimer timer = new AnimationTimer()
        {
            @Override

            public void handle(long now) {
                if(beingPlayed== false)
                    super.stop();
                 appTemplate.getGUI().getPrimaryScene().setOnKeyTyped((KeyEvent event) -> {
                    if(!event.getCharacter().isEmpty()) {
                        char guess = event.getCharacter().toLowerCase().charAt(0);
                        if ((guess >= 'a' && guess <= 'z') || (guess >= 'A' && guess <= 'Z')) {
                            int count = 0;
                            if (!alreadyGuessed(guess) && beingPlayed) {
                                savable = true;
                                appTemplate.getGUI().updateWorkspaceToolbar(savable);
                                boolean goodguess = false;
                                for (Node n : letters) {
                                    Letter letter = (Letter) n;
                                    if (letter.isEqualTo(guess)) {
                                        if(count == 0)
                                        {
                                            lettersUsed.add(new GoodLetterUsed(guess));
                                        }
                                        count++;

                                        letter.show();
                                        goodguess = true;
                                        discovered++;
                                        gamedata.addGoodGuess(guess);
                                    }

                                }
                                if (!goodguess && beingPlayed) {
                                    count = 0;
                                    gamedata.addBadGuess(guess);
                                    lettersUsed.add(new BadLetterUsed(guess));
                                    Workspace.hanger.takeAwayLife();


                                }

                                success = (discovered == gamedata.getTargetWord().length());
                                remains.setText(Integer.toString(gamedata.getRemainingGuesses()));

                            }
                        } else {
                            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                            dialog.show("!", "Only input letters please");

                        }
                    }
                    else
                    {
                        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                        dialog.show("!", "Only input letters please");
                    }
                });
                if (gamedata.getRemainingGuesses() <= 0 || success)
                    stop();
            }

            @Override
            public void stop() {
                super.stop();
                end();
            }
        };
        timer.start();

    }

    private boolean alreadyGuessed(char c) {
        return gamedata.getGoodGuesses().contains(c) || gamedata.getBadGuesses().contains(c);
    }

    @Override
    public void handleNewRequest() {
        AppMessageDialogSingleton messageDialog   = AppMessageDialogSingleton.getSingleton();
        PropertyManager           propertyManager = PropertyManager.getManager();
        boolean                   makenew         = true;
        loadedFromNull = false;
        appTemplate.getGUI().updateWorkspaceToolbar(savable);
        if (savable)
            try {
                makenew = promptToSave();
            } catch (IOException e) {
                messageDialog.show(propertyManager.getPropertyValue(NEW_ERROR_TITLE), propertyManager.getPropertyValue(NEW_ERROR_MESSAGE));
            }
        if (makenew) {
            appTemplate.getDataComponent().reset();                // reset the data (should be reflected in GUI)
            appTemplate.getWorkspaceComponent().reloadWorkspace(); // load data into workspace
            ensureActivatedWorkspace();                            // ensure workspace is activated
            workFile = null;                                       // new workspace has never been saved to a file
            beingPlayed = false;
            Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameWorkspace.reinitialize();
            savable = false;
            appTemplate.getGUI().updateWorkspaceToolbar(savable);
            enableGameButton();
            disableHintButton();
            Workspace.hanger.reset();
        }

        if (gameover) {
            savable = false;
            beingPlayed = false;
            appTemplate.getGUI().updateWorkspaceToolbar(savable);
            Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameWorkspace.reinitialize();
            enableGameButton();
        }

    }
    private void saveWork(File selectedFile) throws IOException {
        //GameData newData = (GameData) appTemplate.getDataComponent();

        appTemplate.getFileComponent().saveData(gamedata, Paths.get(selectedFile.getAbsolutePath()));

        currentWorkFile = selectedFile;
        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
        PropertyManager           props  = PropertyManager.getManager();
        dialog.show(props.getPropertyValue(SAVE_COMPLETED_TITLE), props.getPropertyValue(SAVE_COMPLETED_MESSAGE));
    }

    private void loadWork(File selectedFile) throws IOException
    {
        try {
            appTemplate.getFileComponent().loadData(gamedata, Paths.get(selectedFile.getAbsolutePath()));

            currentWorkFile = selectedFile;

            Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameWorkspace.reinitialize();

            startWhenLoaded();
        }
        catch(IOException e)
        {
            AppMessageDialogSingleton error = AppMessageDialogSingleton.getSingleton();
            error.show("Load error", "An error occurred loading the game");
        }
    }
    @Override
    public void handleLoadRequest()
    {
        /**
         *
         * The below if statement is implemented to make loading work when the game is first started
         *
         **/
        if(loadedFromNull == true)
        {
            appTemplate.getDataComponent().reset();                // reset the data (should be reflected in GUI)
            appTemplate.getWorkspaceComponent().reloadWorkspace(); // load data into workspace
            ensureActivatedWorkspace();                            // ensure workspace is activated
            workFile = null;                                       // new workspace has never been saved to a file
            beingPlayed = false;
            Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameWorkspace.reinitialize();
            savable = false;
            appTemplate.getGUI().updateWorkspaceToolbar(savable);
            enableGameButton();
            Workspace.hanger.reset();
            loadedFromNull = false;
        }
        boolean makenew = true;
        if(savable)
        {
            try
            {
                makenew = promptToSave();
            }
            catch(IOException e)
            {
                AppMessageDialogSingleton newDialog = AppMessageDialogSingleton.getSingleton();
                newDialog.show("Error", "Error loading/saving");
            }
        }
        if(makenew) {
            PropertyManager propertyManager = PropertyManager.getManager();
            try {
                FileChooser fileChooser = new FileChooser();

                FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("JSON file (*.json)", "*.json");
                fileChooser.getExtensionFilters().add(extFilter);

                File selectedFile = fileChooser.showOpenDialog(appTemplate.getGUI().getWindow());


                if (selectedFile != null) {
                    loadWork(selectedFile);
                    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                    if(gamedata.getTargetWord()== null)
                    {
                        AppMessageDialogSingleton dialog2 = AppMessageDialogSingleton.getSingleton();
                        dialog2.show("Load error", propertyManager.getPropertyValue("An error occured loading the game."));
                    }
                    dialog.show("!", "Loading complete");
                    savable = false;
                    appTemplate.getGUI().updateWorkspaceToolbar(savable);
                    loadedFromNull = false;
                } else if (selectedFile == null)
                {

                }

            } catch (IOException ioe) {
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show("Load error", propertyManager.getPropertyValue("An error occured loading the game."));
            }
        }
    }


    @Override
    public void handleSaveRequest() throws IOException {

        PropertyManager propertyManager = PropertyManager.getManager();

        try {
            /**
             *

             if(currentWorkFile!= null)
             {
             saveWork(currentWorkFile);
             savable = false;
             appTemplate.getGUI().updateWorkspaceToolbar(savable);
             }
             **/

            FileChooser fileChooser = new FileChooser();
            File userDirectory = new File(System.getProperty("user.dir") + "/Hangman/saved");


            fileChooser.setInitialDirectory(userDirectory);

            fileChooser.setTitle(propertyManager.getPropertyValue(SAVE_WORK_TITLE));
            FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("JSON file (*.json)", "*.json");
            fileChooser.getExtensionFilters().add(extFilter);


            File selectedFile = fileChooser.showSaveDialog(appTemplate.getGUI().getWindow());
            if (selectedFile != null)
                saveWork(selectedFile);
            savable = false;
            appTemplate.getGUI().updateWorkspaceToolbar(savable);
            if(selectedFile == null)
            {
                savable = true;
                appTemplate.getGUI().updateWorkspaceToolbar(savable);
            }



        } catch (IOException ioe) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(propertyManager.getPropertyValue(SAVE_ERROR_TITLE), propertyManager.getPropertyValue(SAVE_ERROR_MESSAGE));
        }
    }


    @Override
    public void handleExitRequest() {

        YesNoCancelDialogSingleton dialogue = YesNoCancelDialogSingleton.getSingleton();
        if(!beingPlayed)
        {
            dialogue.show("!", "Are you sure you want to exit?");
            if (dialogue.getSelection().equalsIgnoreCase("Yes")) {
                System.exit(0);
            }
            else
            {

            }
        }
        else if(gamedata.getRemainingGuesses() > 0)
        {
            dialogue.show("!", "Are you sure you want to exit? All unsaved progress will be lost");
            if(dialogue.getSelection().equalsIgnoreCase("Yes"))
            {
                System.exit(0);
            }
        }
        else {
            dialogue.show("!", "Are you sure you want to exit?");
            if (dialogue.getSelection().equalsIgnoreCase("Yes")) {
                System.exit(0);
            }
            else
            {

            }
        }

    }

    private void ensureActivatedWorkspace() {
        appTemplate.getWorkspaceComponent().activateWorkspace(appTemplate.getGUI().getAppPane());
    }

    private boolean promptToSave() throws IOException
    {

        PropertyManager            propertyManager   = PropertyManager.getManager();
        YesNoCancelDialogSingleton yesNoCancelDialog = YesNoCancelDialogSingleton.getSingleton();

        yesNoCancelDialog.show(propertyManager.getPropertyValue(SAVE_UNSAVED_WORK_TITLE),
                propertyManager.getPropertyValue(SAVE_UNSAVED_WORK_MESSAGE));

        if (yesNoCancelDialog.getSelection().equals(YesNoCancelDialogSingleton.YES))
        {

            handleSaveRequest();
        }

        return !yesNoCancelDialog.getSelection().equals(YesNoCancelDialogSingleton.CANCEL);

    }

    /**
     * A helper method to save work. It saves the work, marks the current work file as saved, notifies the user, and
     * updates the appropriate controls in the user interface
     *
     * @param target The file to which the work will be saved.
     * @throws IOException
     */
    private void save(Path target) throws IOException
    {

    }
}