package gui;

import apptemplate.AppTemplate;
import components.AppWorkspaceComponent;
import controller.HangmanController;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ToolBar;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.text.Font;
import propertymanager.PropertyManager;
import ui.AppGUI;

import java.io.IOException;

import static hangman.HangmanProperties.*;

/**
 * This class serves as the GUI component for the Hangman game.
 *
 * @author Ritwik Banerjee
 */
public class Workspace extends AppWorkspaceComponent {

    AppTemplate app; // the actual application
    AppGUI      gui; // the GUI inside which the application sits

    Label      guiHeadingLabel;   // workspace (GUI) heading label
    HBox       headPane;          // conatainer to display the heading
    VBox       bodyPane;          // container for the main game displays
    ToolBar    footToolbar;       // toolbar for game buttons
    ToolBar    belowFootToolbar;
    BorderPane figurePane;        // container to display the namesake graphic of the (potentially) hanging person
    VBox       gameTextsPane;     // container to display the text-related parts of the game
    HBox       guessedLetters;    // text area displaying all the letters guessed so far
    HBox       lettersUsedHBox;
    HBox       lettersUsedLabel;
    HBox       remainingGuessBox; // container to display the number of remaining guesses
    Button     startGame;         // the button to start playing a game of Hangman
    Button     hintButton;
    public static HangmanImage hanger = new HangmanImage();


    private static final Font DEFAULT_FONT = new Font("Courier", 36);


    /**
     * Constructor for initializing the workspace, note that this constructor
     * will fully setup the workspace user interface for use.
     *
     * @param initApp The application this workspace is part of.
     * @throws IOException Thrown should there be an error loading application
     *                     data for setting up the user interface.
     */
    public Workspace(AppTemplate initApp) throws IOException {
        app = initApp;
        gui = app.getGUI();
        layoutGUI();     // initialize all the workspace (GUI) components including the containers and their layout
        setupHandlers(); // ... and set up event handling
    }

    public static class HangmanImage extends Parent{
        static final int SPINE_START_X = 110;
        static final int SPINE_START_Y = 20;
        static final int SPINE_END_X = SPINE_START_X;
        static final int SPINE_END_Y = SPINE_START_Y + 60;
        /**
         * How many lives left
         */
        private SimpleIntegerProperty lives = new SimpleIntegerProperty();

        public HangmanImage() {


            Line gallowBar1 = new Line();
            gallowBar1.setStrokeWidth(7);
            gallowBar1.setStroke(Color.RED);
            gallowBar1.setStartX(0);
            gallowBar1.setStartY(110);
            gallowBar1.setEndX(75);
            gallowBar1.setEndY(110);
            gallowBar1.setTranslateX(0);

            Line gallowBar2 = new Line();
            gallowBar2.setStrokeWidth(7);
            gallowBar2.setStroke(Color.RED);
            gallowBar2.setStartX(40);
            gallowBar2.setStartY(-40);
            gallowBar2.setEndX(40);
            gallowBar2.setEndY(110);
            gallowBar2.setTranslateX(0);

            Line gallowBar3 = new Line();
            gallowBar3.setStrokeWidth(7);
            gallowBar3.setStroke(Color.RED);
            gallowBar3.setStartX(40);
            gallowBar3.setStartY(-40);
            gallowBar3.setEndX(105);
            gallowBar3.setEndY(-40);
            gallowBar3.setTranslateX(0);

            Line aboveHeadBar = new Line();
            aboveHeadBar.setStrokeWidth(5);
            aboveHeadBar.setStroke(Color.RED);
            aboveHeadBar.setStartX(110);
            aboveHeadBar.setStartY(-40);
            aboveHeadBar.setEndX(110);
            aboveHeadBar.setEndY(-20);
            // aboveHeadBar.setTranslateX(0);

            Circle head = new Circle(20);
            head.setTranslateX(SPINE_START_X);

            Line spine = new Line();
            spine.setStrokeWidth(5);
            spine.setStartX(SPINE_START_X);
            spine.setStartY(SPINE_START_Y);
            spine.setEndX(SPINE_END_X);
            spine.setEndY(SPINE_END_Y);

            Line rightArm = new Line();
            rightArm.setStrokeWidth(5);
            rightArm.setStartX(SPINE_START_X);
            rightArm.setStartY(SPINE_START_Y + 10);
            rightArm.setEndX(SPINE_START_X + 40);
            rightArm.setEndY(SPINE_START_Y + 20);

            Line leftArm = new Line();
            leftArm.setStrokeWidth(5);
            leftArm.setStartX(SPINE_START_X);
            leftArm.setStartY(SPINE_START_Y + 10);
            leftArm.setEndX(SPINE_START_X - 40);
            leftArm.setEndY(SPINE_START_Y + 20);

            Line rightLeg = new Line();
            rightLeg.setStrokeWidth(5);
            rightLeg.setStartX(SPINE_END_X);
            rightLeg.setStartY(SPINE_END_Y);
            rightLeg.setEndX(SPINE_END_X + 25);
            rightLeg.setEndY(SPINE_END_Y + 50);

            Line leftLeg = new Line();
            leftLeg.setStrokeWidth(5);
            leftLeg.setStartX(SPINE_END_X);
            leftLeg.setStartY(SPINE_END_Y);
            leftLeg.setEndX(SPINE_END_X - 25);
            leftLeg.setEndY(SPINE_END_Y + 50);
            getChildren().addAll(gallowBar1, gallowBar2, gallowBar3, aboveHeadBar, head, spine, leftArm, rightArm, leftLeg, rightLeg);

            lives.set(getChildren().size());
        }

        public void reset() {
            getChildren().forEach(node -> node.setVisible(false));
            lives.set(getChildren().size());
        }

        public void takeAwayLife() {
            for (Node n : getChildren()) {
                if (!n.isVisible()) {
                    n.setVisible(true);
                    lives.set(lives.get() - 1);
                    break;
                }
            }
        }


    }



    private void layoutGUI() {
        PropertyManager propertyManager = PropertyManager.getManager();
        guiHeadingLabel = new Label(propertyManager.getPropertyValue(WORKSPACE_HEADING_LABEL));

        headPane = new HBox();
        headPane.getChildren().add(guiHeadingLabel);
        headPane.setAlignment(Pos.CENTER);


        figurePane = new BorderPane();
        HBox HangMan = new HBox(hanger); //borderPane with HBox ofo Hangman guy inside
        HangMan.setAlignment(Pos.CENTER);
        figurePane.setPadding(new Insets(5, 0, 0, 0));
        figurePane.setCenter(HangMan);

        guessedLetters = new HBox();
        HangmanController.letters = guessedLetters.getChildren(); //HBox of guessed Letters (i.e. rectangles)
        guessedLetters.setAlignment(Pos.CENTER);
        remainingGuessBox = new HBox(); //HBox (remaining bad guesses: )
        remainingGuessBox.setAlignment(Pos.CENTER);
        gameTextsPane = new VBox();
        gameTextsPane.getChildren().setAll(remainingGuessBox, guessedLetters);



        bodyPane = new VBox();
        bodyPane.setAlignment(Pos.CENTER);
        bodyPane.getChildren().addAll(figurePane, gameTextsPane);

        startGame = new Button("Start Playing");
        HBox blankBoxLeft  = new HBox();
        HBox blankBoxRight = new HBox();
        HBox.setHgrow(blankBoxLeft, Priority.ALWAYS);
        HBox.setHgrow(blankBoxRight, Priority.ALWAYS);
        footToolbar = new ToolBar(blankBoxLeft, startGame, blankBoxRight);


        hintButton = new Button("Hint");
        HBox blankRight = new HBox();
        HBox blankLeft = new HBox();
        HBox.setHgrow(blankRight, Priority.ALWAYS);
        HBox.setHgrow(blankLeft, Priority.ALWAYS);
        hintButton.setVisible(false);
        belowFootToolbar = new ToolBar(blankLeft, hintButton, blankRight);

        lettersUsedLabel = new HBox();
        lettersUsedLabel.setAlignment(Pos.CENTER);
        lettersUsedHBox = new HBox();
        HangmanController.lettersUsed = lettersUsedHBox.getChildren();
        lettersUsedHBox.setAlignment(Pos.CENTER);



        workspace = new VBox();
        workspace.getChildren().addAll(headPane, footToolbar, bodyPane, lettersUsedLabel, lettersUsedHBox, belowFootToolbar);
    }

    private void setupHandlers() {
        HangmanController controller = new HangmanController(app, startGame, hintButton);
        startGame.setOnMouseClicked(e -> controller.start());
        hintButton.setOnMouseClicked(e -> controller.handleHint());
    }

    /**
     * This function specifies the CSS for all the UI components known at the time the workspace is initially
     * constructed. Components added and/or removed dynamically as the application runs need to be set up separately.
     */
    @Override
    public void initStyle() {
        PropertyManager propertyManager = PropertyManager.getManager();

        gui.getAppPane().setId(propertyManager.getPropertyValue(ROOT_BORDERPANE_ID));
        gui.getToolbarPane().getStyleClass().setAll(propertyManager.getPropertyValue(SEGMENTED_BUTTON_BAR));
        gui.getToolbarPane().setId(propertyManager.getPropertyValue(TOP_TOOLBAR_ID));

        ObservableList<Node> toolbarChildren = gui.getToolbarPane().getChildren();
        toolbarChildren.get(0).getStyleClass().add(propertyManager.getPropertyValue(FIRST_TOOLBAR_BUTTON));
        toolbarChildren.get(toolbarChildren.size() - 1).getStyleClass().add(propertyManager.getPropertyValue(LAST_TOOLBAR_BUTTON));

        workspace.getStyleClass().add(CLASS_BORDERED_PANE);
        guiHeadingLabel.getStyleClass().setAll(propertyManager.getPropertyValue(HEADING_LABEL));

    }

    /** This function reloads the entire workspace */
    @Override
    public void reloadWorkspace() {

    }

    public VBox getGameTextsPane() {
        return gameTextsPane;
    }

    public HBox getRemainingGuessBox() {
        return remainingGuessBox;
    }

    public HBox getLettersUsedLabel() {
        return lettersUsedLabel;
    }


    public Button getStartGame() {
        return startGame;
    }

    public Button getHintButton()
    {
        return hintButton;
    }

    public void reinitialize() {
        guessedLetters = new HBox();
        guessedLetters.setStyle("-fx-background-color: transparent;");
        guessedLetters.setAlignment(Pos.CENTER);
        guessedLetters.setPadding(new Insets(0, 0, 5, 0));
        HangmanController.letters = guessedLetters.getChildren();


        lettersUsedHBox = new HBox();
        lettersUsedHBox.setAlignment(Pos.CENTER);
        lettersUsedHBox.setPadding(new Insets(0, 0, 5, 0));

        HangmanController.lettersUsed = lettersUsedHBox.getChildren();

        remainingGuessBox = new HBox();
        remainingGuessBox.setAlignment(Pos.CENTER);
        hanger.setVisible(false);

        lettersUsedLabel = new HBox();
        lettersUsedLabel.setAlignment(Pos.CENTER);

        hintButton.setVisible(false);

        gameTextsPane = new VBox();
        gameTextsPane.getChildren().setAll(remainingGuessBox, guessedLetters, lettersUsedLabel, lettersUsedHBox);
        bodyPane.getChildren().setAll(figurePane, gameTextsPane);
    }
}