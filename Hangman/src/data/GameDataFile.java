package data;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.oracle.javafx.jmx.json.JSONWriter;
import components.AppDataComponent;
import components.AppFileComponent;
import ui.AppMessageDialogSingleton;

import java.io.File;
import java.io.IOException;
import java.util.*;



import java.io.IOException;
import java.nio.file.Path;


/**
 * @author Charles Vinueza
 * @author Ritwik Banerjee
 */
public class GameDataFile implements AppFileComponent {

    public static final String TARGET_WORD  = "TARGET_WORD";
    public static final String GOOD_GUESSES = "GOOD_GUESSES";
    public static final String BAD_GUESSES  = "BAD_GUESSES";
    public static GameData loadedData;

    @Override
    public void saveData(AppDataComponent data, Path to)
    { //save json
        GameData gm = (GameData) data;
        ObjectMapper mapper = new ObjectMapper();

        GameData toSave = new GameData();

        toSave.setTargetWord(gm.getTargetWord());
        toSave.setRemainingGuesses(gm.getRemainingGuesses());
        toSave.setBadGuesses(gm.getBadGuesses());
        toSave.setGoodGuesses(gm.getGoodGuesses());
        toSave.setAlreadyGivenHint(gm.getAlreadyGivenHint());


        try {
            //Convert object to JSON string and save into file directly
            mapper.writerWithDefaultPrettyPrinter().writeValue(new File(to.toString()), toSave);

        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    @Override
    public void loadData(AppDataComponent data, Path from) throws IOException
    {
        ObjectMapper mapper2 = new ObjectMapper();
        try {

            // Convert JSON string from file to Object
            loadedData = mapper2.readValue(new File(from.toString()), GameData.class);
        }

        catch (Exception e) {
            AppMessageDialogSingleton error = AppMessageDialogSingleton.getSingleton();
            error.show("Load error", "An error occurred loading the game");
        }

    }
    /** This method will be used if we need to export data into other formats. */
    @Override
    public void exportData(AppDataComponent data, Path filePath) throws IOException
    {

    }
}
