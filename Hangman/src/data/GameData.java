package data;

import apptemplate.AppTemplate;
import components.AppDataComponent;
import controller.GameError;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.stream.Stream;

/**
 * @author Charles Vinueza
 * @author Ritwik Banerjee
 */
public class GameData implements AppDataComponent {

    public static final  int TOTAL_NUMBER_OF_GUESSES_ALLOWED = 10;
    private static final int TOTAL_NUMBER_OF_STORED_WORDS    = 330622;

    private String         targetWord;
    private Set<Character> goodGuesses;
    private Set<Character> badGuesses;
    private int            remainingGuesses;
    public  AppTemplate    appTemplate;
    private int numGoodGuesses;
    private int numDiscovered;
    private boolean alreadyGivenHint;

    public GameData(AppTemplate appTemplate) {
        this.appTemplate = appTemplate;
        this.targetWord = setTargetWord();
        this.goodGuesses = new HashSet<>();
        this.badGuesses = new HashSet<>();
        this.remainingGuesses = TOTAL_NUMBER_OF_GUESSES_ALLOWED;
        this.numGoodGuesses = getNumGoodGuesses();
        this.alreadyGivenHint = false;
    }

    public GameData()
    {

    }

    @Override
    public void reset() {
        this.targetWord = null;
        this.goodGuesses = new HashSet<>();
        this.badGuesses = new HashSet<>();
        this.remainingGuesses = TOTAL_NUMBER_OF_GUESSES_ALLOWED;
        appTemplate.getWorkspaceComponent().reloadWorkspace();
    }
    public void setAppTemplate(AppTemplate app)
    {
        this.appTemplate = app;
    }

    public int getNumDiscovered()
    {
        int sumDiscovered = 0;
        for(char c: goodGuesses)
        {
            for(int i = 0;i<targetWord.length();i++)
            {
                if(targetWord.charAt(i) == c)
                {
                    sumDiscovered++;
                }
            }
        }
        return sumDiscovered;
    }

    public boolean isAlreadyGivenHint()
    {
        return alreadyGivenHint;
    }

    public String getTargetWord() {
        return targetWord;
    }

    private String setTargetWord() {
        URL wordsResource = getClass().getClassLoader().getResource("words/words.txt");
        assert wordsResource != null;

        String s = generateRandomWord();
        while(s.contains("\'") || s.contains("\""))
        {
            System.out.println(s); // just to make sure this loop is working
            s = generateRandomWord();
        }

        return s;
    }
    public String generateRandomWord()
    {
        URL wordsResource = getClass().getClassLoader().getResource("words/words.txt");
        assert wordsResource != null;

        int toSkip = new Random().nextInt(TOTAL_NUMBER_OF_STORED_WORDS);

        try (Stream<String> lines = Files.lines(Paths.get(wordsResource.toURI())))
        {
            return lines.skip(toSkip).findFirst().get();
        }
        catch(IOException | URISyntaxException e)
        {
            e.printStackTrace();
            System.exit(1);
        }
        throw new GameError("Unable to load target word");
    }

    public int getNumGoodGuesses()

    {
        return goodGuesses.size();
    }

    public GameData setTargetWord(String targetWord) {
        this.targetWord = targetWord;
        return this;
    }


    public Set<Character> getGoodGuesses() {
        return goodGuesses;
    }

    @SuppressWarnings("unused")
    public GameData setGoodGuesses(Set<Character> goodGuesses) {
        this.goodGuesses = goodGuesses;
        return this;
    }

    public Set<Character> getBadGuesses() {
        return badGuesses;
    }

    @SuppressWarnings("unused")
    public GameData setBadGuesses(Set<Character> badGuesses) {
        this.badGuesses = badGuesses;
        return this;
    }

    public void setAlreadyGivenHint(boolean hint)
    {
        alreadyGivenHint = hint;

    }
    public boolean getAlreadyGivenHint()
    {
        return alreadyGivenHint;

    }


    public int getRemainingGuesses() {
        return remainingGuesses;
    }

    public void setRemainingGuesses(int i)
    {
        this.remainingGuesses = i;
    }

    public void addGoodGuess(char c) {
        goodGuesses.add(c);
    }

    public void addBadGuess(char c) {
        if (!badGuesses.contains(c)) {
            badGuesses.add(c);
            remainingGuesses--;
        }
    }


}
